﻿using System;

namespace ListaDeExercicios
{
    class Program
    {
        static void Main (string[] args)
        {
            Console.WriteLine("Programa que apresenta os Descontos de seu Salario\n");
            Console.WriteLine(" Digite seu Salario:");
            string texto = Console.ReadLine();

            double salario = Double.Parse(texto);

            double irs = salario * 0.15;
            Console.WriteLine("");
            
            Console.WriteLine("Valor com retenção Salarial:");
            Console.WriteLine(+ salario + " - " + irs + " = " + (salario - irs));


            double SegSocial = salario * 0.12;
            Console.WriteLine("Valor com  desconto Segurança Social:");
            Console.WriteLine(salario + " - " + SegSocial + " = " + (salario - SegSocial));

            Console.ReadKey();
        }

    }

}


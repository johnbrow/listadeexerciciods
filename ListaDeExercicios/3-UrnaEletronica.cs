﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListaDeExercicios
{
    class UrnaEletronica
    {
        static void Main(string[] args)
        {

            int cadto1 = 0;
            int cadto2 = 0;
            int cadto3 = 0;
            int cadto4 = 0;
            int nulo = 0;
            int voto = 1;

            while (voto > 0)
            {
                voto = --voto;
                Console.WriteLine("Digite o numero para seu candidato: \n CANDIDATO1 - 1 \n CANDIDATO2 - 2\n " +
                                  "CANDIDATO3 - 3\n CANDIDATO4 - 4\n APERTE 0 PARA FINALIZAR");
                string votoS = Console.ReadLine();
                voto = int.Parse(votoS);

               
                if (voto == 1)
                {
                    cadto1 = cadto1 + 1;
                }
                else if (voto == 2)
                {
                    cadto2 = cadto2 + 1;
                }
                else if (voto == 3)
                {
                    cadto3 = cadto3 + 1;
                }
                else if (voto == 4)
                {
                    cadto4 = cadto4 + 1;
                }

                else if (voto != 0)
                {

                   nulo = nulo+ 1;

                }

            }


            if (cadto1 > cadto2 && cadto1 > cadto3 && cadto1 > cadto4)
            {
                double total = cadto1 + cadto2 + cadto3 + cadto4 + nulo;
                double porcentagem = 100 * (cadto1/ total);
                porcentagem = Math.Round(porcentagem);

                Console.WriteLine("O CANDIDATO Nº1 FOI O MAIS VOTADO COM: " + cadto1 + " votos");
                Console.WriteLine("PORCENTAGEM DE VOTOS DO VENCENDOR: "+ porcentagem + "%");
            }
            else if (cadto2 > cadto1 && cadto2 > cadto3 && cadto2 > cadto4)
            {
                double total = cadto1 + cadto2 + cadto3 + cadto4 + nulo;
                double porcentagem = 100 * (cadto2 / total);
                porcentagem = Math.Round(porcentagem);

                Console.WriteLine("O CANDIDATO Nº2 FOI O MAIS VOTADO COM: " + cadto2 + " votos");
                Console.WriteLine("PORCENTAGEM DE VOTOS DO VENCENDOR: " + porcentagem + "%");
            }
            else if (cadto3 > cadto1 && cadto3 > cadto2 && cadto3 > cadto4)
            {
                double total = cadto1 + cadto2 + cadto3 + cadto4 + nulo;
                double porcentagem = 100 * (cadto3 / total);
                porcentagem = Math.Round(porcentagem);

                Console.WriteLine("O CANDIDATO Nº3 FOI O MAIS VOTADO COM: " + cadto3 + " votos");
                Console.WriteLine("PORCENTAGEM DE VOTOS DO VENCENDOR: " + porcentagem + "%");

            }
            else if (cadto4 > cadto1 && cadto4 > cadto2 && cadto4 > cadto3)
            {
                double total = cadto1 + cadto2 + cadto3 + cadto4 + nulo;
                double porcentagem = 100 * (cadto4 / total);
                porcentagem = Math.Round(porcentagem);

                Console.WriteLine("O CANDIDATO Nº4 FOI O MAIS VOTADO COM: " + cadto4 + " votos");
                Console.WriteLine("PORCENTAGEM DE VOTOS DO VENCENDOR: " + porcentagem + "%");
            }
            else
            {
                Console.WriteLine("HOUVE UM EMPATE ENTRE OS CANDIDATOS");
            }

            Console.WriteLine("VOTOS NULOS " + nulo);
            Console.ReadKey();

        }


    }
}

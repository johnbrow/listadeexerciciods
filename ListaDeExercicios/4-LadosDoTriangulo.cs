﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListaDeExercicios
{
    class _4_LadosDoTriangulo
    {
        
        static void Main (string[] args)
        {

            double a;
            double b;
            double c;

            // PEDINDO PARA O USUARIO INSERIR OS LADOS DO TRIANGULO
            Console.WriteLine("INSIRA O 1º LADO DO TRIÂNGULO");
            a = Double.Parse(Console.ReadLine());

            Console.WriteLine("INSIRA O 2º LADO DO TRIÂNGULO");
            b = Double.Parse(Console.ReadLine());

            Console.WriteLine("INSIRA O 3º LADO DO TRIÂNGULO");
            c = Double.Parse(Console.ReadLine());


            // a soma dos dois lados menores é menor que o lado maior ou um lado é menor ou igual a zero.
            if ( (a == 0 && b == 0 && c == 0) || ((a + b) < c) || ((c+a) < b) || ((b+c) < a) )
            {
                Console.WriteLine("NÃO E UM TRIANGULO");
                Console.ReadKey();
            }
            // VERIFICANDO SE TODOS SAO DIFERENTES E DIFERENTE DE ZERO

            else if (a != b && a != c && c != b)
                {
                    Console.WriteLine("E UM TRIANGULO ESCALENO");
                    Console.ReadKey();
                }
            

            // SE DOIS LADOS SAO IGUAIS E UM DIFERENTE E DIFERENTE DE ZERO
            
              else if ((a == b && b != c) || (c == b && b != a) || (c == a && a != b ))
                    {
                        Console.WriteLine("E UM TRIANGULO ISÓCELES");
                        Console.ReadKey();
                    }


            //COMPARA SE TODOS OS TRIANGULOS SAO IGUAIS E DIFERENTE DE ZERO

             else  if (a == b && b == c)
                 {
                    Console.WriteLine("E UM TRIANGULO EQUILATERO ");
                    Console.ReadKey();
                 }


            

        }
    }
}

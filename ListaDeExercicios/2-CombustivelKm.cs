﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListaDeExercicios
{
    class _2_CombustivelKm
    {
        //Desculpa professor mas essa questao nao conseguir entender se era pro usuario inserir quantos km,
        //ele percorre ou e pra usar os 100 km que estava em "()", essa questao nao foi muito clara no seu objetivo.
        static void Main(string[] args)
        {
            Console.WriteLine("PROGRAMA APRESENTA QUANTO VC VAI GASTAR COM COMBUSTIVEL\n");

            Console.WriteLine("Digite quantos km seu carro percorre por litro?");
            string texto1 = Console.ReadLine();
            double kmPorLitro = Convert.ToDouble(texto1);

            Console.WriteLine("Quanto é o Litro do Combustivel? ");
            string texto2 = Console.ReadLine();
            double ValorLitro = Convert.ToDouble(texto2);

            double percorreu = 100 / ValorLitro ;
            double gasto = percorreu * kmPorLitro;

            gasto = Math.Round(gasto,2);

            Console.WriteLine("");
            Console.WriteLine("em uma viagem de 100 Km/h vc vai gastar: " + gasto);

            Console.ReadKey();

        }
    }
}
